
## End-to-End Entity Linking and Disambiguation leveraging Word and Knowledge Graph Embeddings

```python sparse_matrix_TFIDF.py --topk <K> --file_path data/sqb/sqb_test.json --output_path data/processed/sqb/sqb_top<K>.txt```