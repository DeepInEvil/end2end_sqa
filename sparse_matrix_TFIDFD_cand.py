import json
from multiprocessing import Pool
import multiprocessing
from gensim.corpora import Dictionary
from gensim.models import TfidfModel
from gensim.similarities import SparseMatrixSimilarity
from tqdm import tqdm
from functools import partial
from argparse import ArgumentParser
import re
import sys
import spacy
import os
from loguru import logger
from gensim.parsing.preprocessing import remove_stopwords

def get_args():
    parser = ArgumentParser(description="TFIDF")
    parser.add_argument('--topk', type=int, required=True)
    parser.add_argument('--file_path', type=str, required=True, help="Full path of the testset file")
    parser.add_argument('--output_path', type=str, required=True, help="Full path of the output file")
    parser.add_argument('--op', type=str, default=None, help="set to noun for using noun chunks")
    parser.add_argument('--recompute', action='store_true')
    return parser.parse_args()

def config_logger():
    """
    configure format for logger
    """
    logger.add("logs/tfidf.log", format="{time} {level} {message}")
    logger.add(sys.stderr, format="{time} {level} {message}", level="INFO")


def getNouns(nlp,text):
    """
    generate noun chunks from text
    """
    doc = nlp(text)
    return ' '.join([np.text for np in doc.noun_chunks])


def entity2list():
    """
    creating entity mapping
    """
    logger.info("Creating entity to list")
    ent2name_list = list()
    with open('data/FB2M/names.trimmed.2M.txt') as fin:
        for line in tqdm(fin,desc="2M names"):
            line = line.strip().split('\t')
            ent2name_list.append((line[0].replace('<', '').replace('>',''), re.sub('[^a-zA-Z0-9\n\.]', ' ', line[2])))
    return ent2name_list


def read_testset(filepah):
    """
    reading test file
    """
    logger.info(f"Reading test file: {filepah}")
    all_data, questions, labels = list(), list(), list()
    if "sqb" in filepah:
        for d in json.load(open(filepah)):
            questions.append(d["question"].split())
            labels.append(d["subject_text"])
            allinfo = d["question"]+"\t"+d["subject_text"]+"\t"+d["subject"]+"\t"+str(d["relation"])+"\t"+d["object"]
            all_data.append(allinfo.split())
    else:
        """
        MODIFY HERE IN ORDER TO BUILD VOCABULARY
        """
        with open(filepah) as fin:
            for line in tqdm(fin,desc="Reading testset: "):
                line = line.strip().split('\t')
                labels.append(line[2].split())
    return labels


def get_cands(entity, entity_mat, topk,ent2list):
    entity_vec = [tfidf[vocab_dict.doc2bow(entity.split())]]
    sims = enumerate(entity_mat.get_similarities(entity_vec))
    sims = sorted(sims, key=lambda x: x[1], reverse=True)[:topk]
    print(len(sims))
    print(sims)
    candidates = ['|'.join(ent2list[c[0]]) for c in sims]
    print("cands> ", candidates)
    return candidates


def get_processed_data(entity_labels,ent2name_list,args):
    """
    compute and save predicted candidate entities to file
    """

    #creating vocabulary and TF-IDF vectors
    logger.info("Creating vocabulary and TF-IDF")
    vocab_dict = Dictionary(entity_labels)
    corpus = [vocab_dict.doc2bow(item[1].split()) for item in tqdm(ent2name_list, desc="building corpus")]
    tfidf = TfidfModel(corpus)

    #computing entity matrix
    logger.info("Computing entity matrix...")
    entity_matrix = SparseMatrixSimilarity(corpus=tfidf[corpus],
                                           num_features=len(vocab_dict.items()))

    return vocab_dict,corpus,tfidf,entity_matrix


if __name__ == '__main__':
    args = get_args()
    config_logger()

    ent2list = entity2list()    # getting entity mapping
    entity_labels = read_testset(args.file_path)  # getting entity labels
    vocab_dict,corpus,tfidf,entity_matrix = get_processed_data(entity_labels,ent2list, args) # proecessing indices over the corpus

    test_entity = "dead combo"
    cands = get_cands(test_entity,entity_mat=entity_matrix,topk=args.topk,ent2list=ent2list)
    logger.info("cands = ",cands)





