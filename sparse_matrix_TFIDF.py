import json
from multiprocessing import Pool
import multiprocessing
from gensim.corpora import Dictionary
from gensim.models import TfidfModel
from gensim.similarities import SparseMatrixSimilarity
from tqdm import tqdm
from functools import partial
from argparse import ArgumentParser
import re
import sys
import spacy
import os
from loguru import logger
from gensim.parsing.preprocessing import remove_stopwords

def get_args():
    parser = ArgumentParser(description="TFIDF")
    parser.add_argument('--topk', type=int, required=True)
    parser.add_argument('--file_path', type=str, required=True, help="Full path of the testset file")
    parser.add_argument('--output_path', type=str, required=True, help="Full path of the output file")
    parser.add_argument('--op', type=str, default=None, help="set to noun for using noun chunks")
    parser.add_argument('--recompute', action='store_true')
    return parser.parse_args()

def config_logger():
    """
    configure format for logger
    """
    logger.add("logs/tfidf.log", format="{time} {level} {message}")
    logger.add(sys.stderr, format="{time} {level} {message}", level="INFO")


def getNouns(nlp,text):
    """
    generate noun chunks from text
    """
    doc = nlp(text)
    return ' '.join([np.text for np in doc.noun_chunks])


def entity2list():
    """
    creating entity mapping
    """
    logger.info("Creating entity to list")
    ent2name_list = list()
    with open('data/freebase/names.trimmed.2M.txt') as fin:
        for line in tqdm(fin,desc="2M names"):
            line = line.strip().split('\t')
            ent2name_list.append((line[0].replace('<', '').replace('>',''), re.sub('[^a-zA-Z0-9\n\.]', ' ', line[2])))
    return ent2name_list


def read_testset(filepah):
    """
    reading test file
    """
    logger.info(f"Reading test file: {filepah}")
    all_data, questions, labels = list(), list(), list()
    if "sqb" in filepah:
        for d in json.load(open(filepah)):
            questions.append(d["question"].split())
            labels.append(d["subject_text"])
            allinfo = d["question"]+"\t"+d["subject_text"]+"\t"+d["subject"]+"\t"+str(d["relation"])+"\t"+d["object"]
            all_data.append(allinfo.split())
    else:
        with open(filepah) as fin:
            for line in tqdm(fin,desc="Reading testset: "):
                line = line.strip().split('\t')
                all_data.append(line[:-1])
                questions.append(line[-2].split())
                labels.append(line[1])
    return all_data,questions,labels


def get_cands(question_vec, entity_mat, topk):

    sims = enumerate(entity_mat.get_similarities(question_vec))
    sims = sorted(sims, key=lambda x: x[1], reverse=True)[:topk]
    return sims


def save_entity_candidates(questions,ent2name_list,args):
    """
    compute and save predicted candidate entities to file
    """

    #creating vocabulary and TF-IDF vectors
    logger.info("Creating vocabulary and TF-IDF")
    vocab_dict = Dictionary(questions)
    corpus = [vocab_dict.doc2bow(item[1].split()) for item in tqdm(ent2name_list, desc="building corpus")]
    tfidf = TfidfModel(corpus)

    #computing entity matrix
    logger.info("Computing entity matrix...")
    entity_matrix = SparseMatrixSimilarity(corpus=tfidf[corpus],
                                           num_features=len(vocab_dict.items()))

    partial_func = partial(get_cands, entity_mat=entity_matrix, topk=args.topk)

    logger.info("Computing candidate relations .... This might take a while. Sit tight")
    with Pool(processes=multiprocessing.cpu_count()-1) as pool:
        result_cands = pool.map(func=partial_func,iterable=tqdm([tfidf[vocab_dict.doc2bow(q)] for q in tqdm(questions)],desc="Pool"))

    flags = list()
    result_string = ''
    for line, cands, label in tqdm(zip(all_data, result_cands, labels),desc="Results"):
        try:
            line_cands = ['|'.join(ent2name_list[c[0]]) for c in cands]
        except:
            logger.error("Error occured !!")
            logger.error(type(cands))
            logger.error(cands)
            exit()
        line.append('\t'.join(line_cands))
        result_string += '\t'.join(line) + '\n'

        flag = label in [c.split('|')[0] for c in line_cands]
        flags.append(flag)
        if not flag:
            logger.info('Line {} has no label in candidates.'.format(line[0]))

    logger.info('{} have label in candidates.'.format(str(sum(flags) / len(flags))))

    with open(args.output_path, 'wt') as fout:
        fout.write(result_string)
        logger.success(f"Saved predictions to {args.output_path}")

if __name__ == '__main__':
    args = get_args()
    config_logger()
    if args.op is not None:
        nlp = spacy.load("en")  # loading spacy language model


    if not args.recompute:
        if os.path.exists(args.output_path):
            logger.error(f"File {args.output_path} already exists !!")
            logger.error(f"To overwrite the existing file try running with additional parameter --recompute")
            exit()

    ent2list = entity2list()    # getting entity mapping
    all_data, questions, labels = read_testset(args.file_path)  # reading testfile

    # compute and save predicted candidate entities to file
    save_entity_candidates(questions,ent2list, args)





